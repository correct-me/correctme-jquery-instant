function isEmpty(str) {
    return (!str || str.length === 0 );
}

class AddressInstantSuggest {

    static instance;

    static create(options) {
        if (AddressInstantSuggest.instance != null) {
            return AddressInstantSuggest.instance;
        } else {
            AddressInstantSuggest.instance = new AddressInstantSuggest(options);
            return AddressInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return AddressInstantSuggest.$j;
    }

    static setJQuery(jq) {
        return AddressInstantSuggest.$j = jq;
    }

    static getUpdated(term, suggestions) {
        if (suggestions.length > 0) {
            AddressInstantSuggest.cache[term] = suggestions;
            return suggestions;
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in AddressInstantSuggest.cache) {
                return AddressInstantSuggest.cache[term];
            }

            let suggestions = [];
            AddressInstantCorrect.$j.ajax({
                url: this._options.host + "/api/v1/suggestAddress?etoken=" + this._options.token + "&address=" + term,
                async: false,
                timeout: 1000
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    data.response.values.forEach(function (a) {
                        suggestions.push({label: a.suggestion});
                    });
                    AddressInstantSuggest.getUpdated(term, suggestions);
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}

class AddressInstantCorrect {

    static instance;

    static create(options) {
        if (AddressInstantCorrect.instance != null) {
            return AddressInstantCorrect.instance;
        } else {
            AddressInstantCorrect.instance = new AddressInstantCorrect(options);
            return AddressInstantCorrect.instance;
        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return AddressInstantCorrect.$j;
    }

    static setJQuery(jq) {
        return AddressInstantCorrect.$j = jq;
    }

    correct(term) {
        if (typeof this._options !== "undefined" && this._options != null) {

            let address_line = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.address_line);
            let zip = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.zip);
            let district = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.district);
            let area = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.area);
            let city = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.city);
            let street = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.street);
            let num = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.number);

            AddressInstantCorrect.$j.get({
                url: this._options.host + "/api/v1/correctAddress?etoken=" + this._options.token + "&address=" + term,
                async: false,
                timeout: 1000
            }).then(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    address_line.attr('class', AddressInstantCorrect.instance._options.verified);
                    address_line.val(data.response.postCode + ", "
                        + data.response.area + " обл., "
                        + data.response.localityType
                        + data.response.locality
                        + ", " + data.response.streetType
                        + data.response.street
                        + ", буд." + data.response.streetNumber);
                    zip.val(data.response.postCode || "");
                    district.val(data.response.area || "");
                    area.val(data.response.region || "");
                    city.val(data.response.localityType + data.response.locality || "");
                    street.val(data.response.streetType + data.response.street || "");
                    num.val(data.response.streetNumber || "");
                } else {
                    address_line.attr('class', AddressInstantCorrect.instance._options.error);
                    address_line.val("");
                    zip.val("");
                    district.val("");
                    area.val("");
                    city.val("");
                    street.val("");
                    num.val("");
                }
            }).fail(function () {
                address_line.attr('class', AddressInstantCorrect.instance._options.error);
                address_line.val("");
                zip.val("");
                district.val("");
                area.val("");
                city.val("");
                street.val("");
                num.val("");
            });
        }
    }
}

class CityInstantSuggest {

    static instance;

    static create(options) {
        if (CityInstantSuggest.instance != null) {
            return CityInstantSuggest.instance;
        } else {
            CityInstantSuggest.instance = new CityInstantSuggest(options);
            return CityInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return CityInstantSuggest.$j;
    }

    static setJQuery(jq) {
        return CityInstantSuggest.$j = jq;
    }

    static getUpdated(term, suggestions) {
        if (suggestions.length > 0) {
            CityInstantSuggest.cache[term] = suggestions;
            return suggestions;
        }
    }

    areaUrlToken(options) {
        if (typeof options.area_id !== "undefined" && options.area_id != null) {
            let area = AddressInstantCorrect.$j('#'+ options.area_id).val();
            // console.log("&area=" + area);
            if (isEmpty(area)) {
                return "";
            } else {
                return "&area=" + area;
            }
        } else {
            if (typeof options.area !== "undefined" && options.area != null) {
                return "&area=" + options.area;
            } else {
                return "";
            }
        }
    }

    regionUrlToken(options) {
        if (typeof options.region_id !== "undefined" && options.region_id != null) {
            let region = AddressInstantCorrect.$j('#'+ options.region_id).val();
            // console.log("&region=" + region);
            if (isEmpty(region)) {
                return "";
            } else {
                return "&region=" + region;
            }
        } else {
            if (typeof options.region !== "undefined" && options.region != null) {
                return "&region=" + options.region;
            } else {
                return "";
            }
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in CityInstantSuggest.cache) {
                return CityInstantSuggest.cache[term];
            }

            let suggestions = [];
            AddressInstantCorrect.$j.ajax({
                url: this._options.host + "/api/v1/suggestCity?etoken=" + this._options.token + this.areaUrlToken(this._options) + this.regionUrlToken(this._options) + "&city=" + term,
                async: false,
                timeout: 1000
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    data.response.values.forEach(function (a) {
                        // console.log(a.suggestion);
                        // console.log(a[CityInstantSuggest.instance._options.field]);
                        // suggestions.push({label: a[CityInstantSuggest.instance._options.field]});
                        suggestions.push({label: a.suggestion});
                    });
                    CityInstantSuggest.getUpdated(term, suggestions);
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}

class StreetInstantSuggest {

    static instance;

    static create(options) {
        if (StreetInstantSuggest.instance != null) {
            return StreetInstantSuggest.instance;
        } else {
            StreetInstantSuggest.instance = new StreetInstantSuggest(options);
            return StreetInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return StreetInstantSuggest.$j;
    }

    static setJQuery(jq) {
        return StreetInstantSuggest.$j = jq;
    }

    static getUpdated(term, suggestions) {
        if (suggestions.length > 0) {
            StreetInstantSuggest.cache[term] = suggestions;
            return suggestions;
        }
    }

    areaUrlToken(options) {
        if (typeof options.area_id !== "undefined" && options.area_id != null) {
            let area = AddressInstantCorrect.$j('#'+ options.area_id).val();
            if (isEmpty(area)) {
                return "";
            } else {
                return "&area=" + area;
            }
        } else {
            if (typeof options.area !== "undefined" && options.area != null) {
                return "&area=" + options.area;
            } else {
                return "";
            }
        }
    }

    regionUrlToken(options) {
        if (typeof options.region_id !== "undefined" && options.region_id != null && options.region_id != "") {
            let region = AddressInstantCorrect.$j('#'+ options.region_id).val();
            if (isEmpty(region)) {
                return "";
            } else {
                return "&region=" + region;
            }
        } else {
            if (typeof options.region !== "undefined" && options.region != null) {
                return "&region=" + options.region;
            } else {
                return "";
            }
        }
    }

    cityUrlToken(options) {
        if (typeof options.city_id !== "undefined" && options.city_id != null) {
            let city = AddressInstantCorrect.$j('#'+ options.city_id).val();
            if (isEmpty(city)) {
                return "";
            } else {
                return "&city=" + city;
            }
        } else {
            if (typeof options.city !== "undefined" && options.city != null) {
                return "&city=" + options.city;
            } else {
                return "";
            }
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in StreetInstantSuggest.cache) {
                return StreetInstantSuggest.cache[term];
            }

            let suggestions = [];
            AddressInstantCorrect.$j.ajax({
                url: this._options.host + "/api/v1/suggestStreet?etoken=" + this._options.token + this.areaUrlToken(this._options) + this.regionUrlToken(this._options) + this.cityUrlToken(this._options) + "&street=" + term,
                async: false,
                timeout: 1000
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    data.response.values.forEach(function (a) {
                        // console.log(a.suggestion);
                        // console.log(a[StreetInstantSuggest.instance._options.field]);
                        // suggestions.push({label: a[StreetInstantSuggest.instance._options.field]});
                        suggestions.push({label: a.suggestion});
                    });
                    StreetInstantSuggest.getUpdated(term, suggestions);
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}



class FullnameInstantSuggest {

    static instance;

    static create(options) {
        if (FullnameInstantSuggest.instance != null) {
            return FullnameInstantSuggest.instance;
        } else {
            FullnameInstantSuggest.instance = new FullnameInstantSuggest(options);
            return FullnameInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};
    static suggestions_cache = [];
    static suggestions_plain = [];

    static $j = jQuery.noConflict();

    static getJQuery() {
        return FullnameInstantSuggest.$j;
    }

    fill(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (AddressInstantCorrect.$j.inArray(term, FullnameInstantSuggest.suggestions_plain) > -1) {
                let fiog = FullnameInstantSuggest.suggestions_cache[AddressInstantCorrect.$j.inArray(term, FullnameInstantSuggest.suggestions_plain)];
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.fullname).val(fiog.suggestion);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.name).val(fiog.name);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.thirdname).val(fiog.thirdName);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.surname).val(fiog.surName);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.gender).val(fiog.gender);
            }
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in FullnameInstantSuggest.cache) {
                return FullnameInstantSuggest.cache[term];
            }
            let suggestions = [];
            AddressInstantCorrect.$j.ajax({
                url: this._options.host + "/api/v1/suggestFullName?etoken=" + this._options.token + "&name=" + term,//.replace(/ /g,"%20")
                async: false,
                timeout: 1000
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    FullnameInstantSuggest.suggestions_cache = [];
                    FullnameInstantSuggest.suggestions_plain = [];
                    data.response.values.forEach(function (a) {
                        suggestions.push({label: a.suggestion});
                        FullnameInstantSuggest.suggestions_cache.push(a);
                        FullnameInstantSuggest.suggestions_plain.push(a.suggestion);
                    });

                    FullnameInstantSuggest.cache[term] = suggestions;
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}


AddressInstantSuggest.getJQuery()(document).ready(function () {
    AddressInstantSuggest.getJQuery().fn.addressLineInstantSuggest = function (options) {
        const address = AddressInstantSuggest.create(options);
        AddressInstantSuggest.getJQuery()('#' + options.input_id).autocomplete({
            minLength: 1,
            source: function (request, response) {
                let r = address.suggest(request.term);
                response(r);
            }
        });
    };
});

CityInstantSuggest.getJQuery()(document).ready(function () {
    CityInstantSuggest.getJQuery().fn.cityInstantSuggest = function (options) {
        const city = CityInstantSuggest.create(options);
        CityInstantSuggest.getJQuery()('#' + options.input_id).autocomplete({
            minLength: 1,
            source: function (request, response) {
                let r = city.suggest(request.term);
                response(r);
            }
        });
    };
});

StreetInstantSuggest.getJQuery()(document).ready(function () {
    StreetInstantSuggest.getJQuery().fn.streetInstantSuggest = function (options) {
        const street = StreetInstantSuggest.create(options);
        StreetInstantSuggest.getJQuery()('#' + options.input_id).autocomplete({
            minLength: 1,
            source: function (request, response) {
                let r = street.suggest(request.term);
                response(r);
            }
        });
    };
});

AddressInstantCorrect.getJQuery()(document).ready(function () {
    AddressInstantCorrect.getJQuery().fn.addressLineInstantCorrect = function (options) {

        const address = AddressInstantCorrect.create(options);

        AddressInstantCorrect.getJQuery().fn.enterKey = function (fnc) {
            return this.each(function () {
                AddressInstantCorrect.getJQuery()(this).keypress(function (ev) {
                    let keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };

        AddressInstantCorrect.getJQuery()('#' + options.input_id).on('autocompletechange change', function () {
            address.correct(AddressInstantCorrect.getJQuery()(this).val());
        }).change();

        AddressInstantCorrect.getJQuery()('#' + options.button_id).click(function (event) {
            event.preventDefault();
            AddressInstantCorrect.getJQuery()(".ui-menu-item").hide();
            address.correct(AddressInstantCorrect.getJQuery()('#' + id).val());
        });

        AddressInstantCorrect.getJQuery()('#' + options.input_id).enterKey(function () {
            AddressInstantCorrect.getJQuery()(".ui-menu-item").hide();
            address.correct(AddressInstantCorrect.getJQuery()(this).val());
        });

    };
});

FullnameInstantSuggest.getJQuery()(document).ready(function () {
    FullnameInstantSuggest.getJQuery().fn.fullnameLineInstantSuggest = function (options) {
        const fullname = FullnameInstantSuggest.create(options);

        FullnameInstantSuggest.getJQuery()(function () {
            FullnameInstantSuggest.getJQuery()('#' + options.input_id).autocomplete({
                minLength: 2,
                source: function (request, response) {
                    let r = fullname.suggest(request.term);
                    response(r);
                }
            });
        });

        FullnameInstantSuggest.getJQuery().fn.enterKey = function (fnc) {
            return this.each(function () {
                FullnameInstantSuggest.getJQuery()(this).keypress(function (ev) {
                    let keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };

        FullnameInstantSuggest.getJQuery()('#' + options.input_id).enterKey(function () {
            fullname.fill(this.value);
        });

        FullnameInstantSuggest.getJQuery()('#' + options.input_id).on('autocompletechange change', function () {
            fullname.fill(this.value);
        }).change();
    }
});

